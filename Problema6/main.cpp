#include <iostream>
#include <ctype.h>
using namespace std;

int main()
{string palabra_ingresada;
    cout << "Ingrese una palabra para convertir sus letras minusculas en mayusculas: " << endl;
    cin >> palabra_ingresada;
    for (int i=0;i<palabra_ingresada.length();i++){
        palabra_ingresada[i]=toupper(palabra_ingresada[i]);
    }
    cout<< "La palabra en mayuscula es: "<<palabra_ingresada<<endl;
    return 0;
}
